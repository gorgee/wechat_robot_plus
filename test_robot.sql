/*
Navicat MySQL Data Transfer

Source Server         : localhost_mysql
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : test_robot

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2013-05-12 21:26:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `wxmsg_blacklists`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_blacklists`;
CREATE TABLE `wxmsg_blacklists` (
  `user_opn_id` varchar(50) NOT NULL,
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_opn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_blacklists
-- ----------------------------
INSERT INTO `wxmsg_blacklists` VALUES ('ope8Ej2MwmROulagRaGYWWwswfv4', '1368084736');

-- ----------------------------
-- Table structure for `wxmsg_commands`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_commands`;
CREATE TABLE `wxmsg_commands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `command` varchar(50) NOT NULL DEFAULT '',
  `p_cmd_id` int(10) unsigned NOT NULL DEFAULT '0',
  `data_regex` varchar(100) NOT NULL DEFAULT '',
  `is_expire` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_with_plugin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `plugin_name` varchar(100) NOT NULL DEFAULT '',
  `plugin_function` varchar(100) NOT NULL DEFAULT '',
  `reply_msgtype_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `pic_url` text NOT NULL,
  `link_url` text NOT NULL,
  `music_url` text NOT NULL,
  `hqmusic_url` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `command` (`command`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_commands
-- ----------------------------
INSERT INTO `wxmsg_commands` VALUES ('1', 't1', '0', '1', '0', '0', '', '', '1', '测试1', '', '', '', '', '', '');
INSERT INTO `wxmsg_commands` VALUES ('2', 'err_invlid_command', '0', '', '0', '0', '', '', '1', '无效的命令', '', '', '', '', '', '');
INSERT INTO `wxmsg_commands` VALUES ('3', 'err_command_expired', '0', '', '0', '0', '', '', '1', '命令输入超时', '', '', '', '', '', '');
INSERT INTO `wxmsg_commands` VALUES ('4', 'err_incorrect_parent_command', '0', '', '0', '0', '', '', '1', '命令流程错误', '', '', '', '', '', '');
INSERT INTO `wxmsg_commands` VALUES ('8', 'err_incorrect_command_data', '0', '', '0', '0', '', '', '1', '命令参数不正确', '', '', '', '', '', '');
INSERT INTO `wxmsg_commands` VALUES ('9', 'err_save_user_datas_failed', '0', '', '0', '0', '', '', '1', '用户数据保存失败', '', '', '', '', '', '');
INSERT INTO `wxmsg_commands` VALUES ('10', 't2', '1', '2', '0', '0', '', '', '1', '测试2', '', '', '', '', '', '');
INSERT INTO `wxmsg_commands` VALUES ('11', 't3', '10', '3', '0', '0', '', '', '1', '测试3', '', '', '', '', '', '');
INSERT INTO `wxmsg_commands` VALUES ('14', 'testtest', '3', 'testtest', '0', '1', 'test_plugin', 'test_run', '6', 'wawefwaeg', '这是一个测试的标题', '这个也是测试的内容罢了,不要在意~', 'http://www.tt.com/new_weixin_robot/uploads/8930facec1ace2dff34a69410c27a39b.jpg', 'http://www.tt.com', 'qwwg', 'eerghwef');

-- ----------------------------
-- Table structure for `wxmsg_datas`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_datas`;
CREATE TABLE `wxmsg_datas` (
  `user_opn_id` varchar(50) NOT NULL,
  `last_cmd` varchar(50) NOT NULL DEFAULT '',
  `last_cmd_id` int(10) unsigned NOT NULL DEFAULT '0',
  `last_time` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `confirm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_opn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_datas
-- ----------------------------
INSERT INTO `wxmsg_datas` VALUES ('user_opn_id', 't3', '11', '1368156942', '{\"test\":\"123456\",\"t1\":\"1\",\"t2\":\"2\",\"t3\":\"3\"}', '0');

-- ----------------------------
-- Table structure for `wxmsg_keywords`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_keywords`;
CREATE TABLE `wxmsg_keywords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `for_msgtype_id` int(10) unsigned NOT NULL DEFAULT '0',
  `keyword` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_keywords
-- ----------------------------
INSERT INTO `wxmsg_keywords` VALUES ('1', '1', '你好', '测试');
INSERT INTO `wxmsg_keywords` VALUES ('2', '3', 'subscribe', '订阅事件');
INSERT INTO `wxmsg_keywords` VALUES ('3', '1', 'help_contents', '用于绑定帮助信息');

-- ----------------------------
-- Table structure for `wxmsg_msgbox`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_msgbox`;
CREATE TABLE `wxmsg_msgbox` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_opn_id` varchar(50) NOT NULL DEFAULT '',
  `platform_opn_id` varchar(50) NOT NULL DEFAULT '',
  `msgtype_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `post_time` int(10) unsigned NOT NULL DEFAULT '0',
  `wx_msg_id` varchar(50) NOT NULL DEFAULT '0',
  `reply_msgtype_id` int(10) unsigned NOT NULL DEFAULT '0',
  `reply_content` text NOT NULL,
  `reply_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_opn_id` (`user_opn_id`) USING BTREE,
  KEY `platform_opn_id` (`platform_opn_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_msgbox
-- ----------------------------

-- ----------------------------
-- Table structure for `wxmsg_msgtype`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_msgtype`;
CREATE TABLE `wxmsg_msgtype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(10) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `for_reply` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_name` (`type_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_msgtype
-- ----------------------------
INSERT INTO `wxmsg_msgtype` VALUES ('1', 'text', '文本', '1');
INSERT INTO `wxmsg_msgtype` VALUES ('2', 'image', '图片', '0');
INSERT INTO `wxmsg_msgtype` VALUES ('3', 'event', '事件推送', '0');
INSERT INTO `wxmsg_msgtype` VALUES ('4', 'location', '地理位置', '0');
INSERT INTO `wxmsg_msgtype` VALUES ('5', 'link', '超链接', '0');
INSERT INTO `wxmsg_msgtype` VALUES ('6', 'news', '图文超链接', '1');
INSERT INTO `wxmsg_msgtype` VALUES ('7', 'voice', '语音', '0');
INSERT INTO `wxmsg_msgtype` VALUES ('8', 'music', '音频', '1');

-- ----------------------------
-- Table structure for `wxmsg_reply`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_reply`;
CREATE TABLE `wxmsg_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `for_key_id` int(10) unsigned NOT NULL DEFAULT '0',
  `reply_msgtype_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `pic_url` text NOT NULL,
  `link_url` text NOT NULL,
  `music_url` text NOT NULL,
  `hqmusic_url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_reply
-- ----------------------------
INSERT INTO `wxmsg_reply` VALUES ('1', '1', '1', '呵呵，洗澡去了', '', '', '', '', '', '');
INSERT INTO `wxmsg_reply` VALUES ('2', '2', '1', '接收到用户订阅事件接收到用户订阅事件接收到用户订阅事件接收到用户订阅事件接收到用户订阅事件接收到用户订阅事件接收到用户订阅事件接收到用户订阅事件接收到用户订阅事件', '', '', '', '', '', '');
INSERT INTO `wxmsg_reply` VALUES ('4', '1', '6', '啦啦我是一个菠菜菠菜菠菜菠菜菠菜', '这个是标题', '这个是描述', 'http://www.tt.com/new_weixin_robot/uploads/b84843bef1d2ffb18a6b6fc5ff4b93c1.jpg', 'http://www.google.com', 'http://www.tt.com/new_weixin_robot/uploads/cb3e47f363988b4191983520bceee07a.jpg', 'http://www.tt.com/new_weixin_robot/uploads/71d59c14e60b049cbd498d886a3b52b6.jpg');
INSERT INTO `wxmsg_reply` VALUES ('5', '3', '1', '您输入的命令无法识别,请检查后再试~', '', '', '', '', '', '');
INSERT INTO `wxmsg_reply` VALUES ('6', '3', '6', '', '暂时没有办法回复您的信息', '~~~', 'http://www.tt.com/new_weixin_robot/uploads/62164042df5e5cdf13f691a6a0ad4330.jpg', 'http://www.tt.com', '', '');

-- ----------------------------
-- Table structure for `wxmsg_user`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_user`;
CREATE TABLE `wxmsg_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `group` varchar(10) NOT NULL DEFAULT '',
  `create_time` int(10) unsigned NOT NULL,
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_user
-- ----------------------------
INSERT INTO `wxmsg_user` VALUES ('6', 'admin0', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '1366986992', '1366989625');
