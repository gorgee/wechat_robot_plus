<div id="win_r">
    <table cellpadding="0" cellspacing="0" class="text_c">
        <tr class="table_title">
            <td style="width: 150px"><?php echo lang('for_msgtype'); ?></td>
            <td style="width: 200px"><?php echo lang('keyword'); ?></td>
            <td style="width: 250px"><?php echo lang('description'); ?></td>
            <td style="width: 100px"><?php echo lang('operation'); ?></td>
        </tr>
        <?php
        if (is_array($keywords)) {
            foreach ($keywords as $v) {
                $tmp = '<tr>'
                        . '<td>' . $v->for_msgtype_name . '</td>'
                        . '<td>' . $v->keyword . '</td>'
                        . '<td>' . $v->description . '</td>'
                        . '<td>'
                        . anchor(site_url('admin/keywords_edit/' . $v->id), lang('edit'))
                        . ' | '
                        . anchor(site_url('admin/keywords_dodel/' . $v->id), lang('delete'), array(
                            'onclick' => 'if(false===confirm(\'' . lang('confirm_to_delete') . '\')){return false;}'
                        ))
                        . '</td>';
                echo $tmp;
            }
        } else {
            echo lang('err_no_data');
        }
        ?>
    </table>
    <?php echo $pages; ?>
</div>