<div id="win_r">
    <ul>
        <li>
            <span class="title"><?php echo lang('command'); ?></span>
            <span class="m_left_10"><?php echo $command->command; ?></span>
        </li>
        <li>
            <span class="title"><?php echo lang('parent_command'); ?></span>
            <span class="m_left_10"><?php echo (empty($command->parent_command) ? lang('none') : $command->parent_command); ?></span>
        </li>
        <li>
            <span class="title"><?php echo lang('data_regex'); ?></span>
            <span class="m_left_10"><?php echo $command->data_regex; ?></span>
        </li>
        <li>
            <span class="title"><?php echo lang('check_expire'); ?></span>
            <span class="m_left_10"><?php echo $command->is_expire ? lang('yes') : lang('no'); ?></span>
        </li>
        <li>
            <span class="title"><?php echo lang('use_plugin'); ?></span>
            <span class="m_left_10"><?php echo $command->is_with_plugin ? lang('yes') : lang('no'); ?></span>
        </li>
        <?php
        if ($command->is_with_plugin) {
            ?>
            <li>
                <ul>
                    <li>
                        <span class="title"><?php echo lang('plugin_name'); ?></span>
                        <span class="m_left_10"><?php echo $command->plugin_name; ?></span>
                    </li>
                    <li>
                        <span class="title"><?php echo lang('plugin_function'); ?></span>
                        <span class="m_left_10"><?php echo $command->plugin_function; ?></span>
                    </li>
                </ul>
            </li>
            <?php
        }
        ?>
        <li>
            <span class="title"><?php echo lang('reply_msgtype'); ?></span>
            <span class="m_left_10"><?php echo $command->reply_msgtype_name; ?></span>

        </li>
        <li>
            <div id="msgtype_text">
                <span class="title"><?php echo lang('reply_content'); ?></span>
                <textarea disabled="disabled" class="input_area"><?php echo $command->content; ?></textarea>
            </div>
            <div id="msgtype_news">
                <ul>
                    <li>
                        <span class="title"><?php echo lang('title'); ?></span>
                        <input type="text" disabled="disabled" value="<?php echo $command->title; ?>" class="wide_input" />
                    </li>
                    <li>
                        <span class="title"><?php echo lang('description'); ?></span>
                        <textarea disabled="disabled" class="input_area"><?php echo $command->description; ?></textarea>
                    </li>
                    <li>
                        <span class="title"><?php echo lang('pic_url'); ?></span>
                        <input type="text" disabled="disabled" value="<?php echo $command->pic_url; ?>" class="wide_input" />
                        <?php
                        if (!empty($command->pic_url)) {
                            echo '<a target="_blank" class="m_left_10" href="' . $command->pic_url . '">'
                            . lang('preview') . '</a>';
                        }
                        ?>
                    </li>
                    <li>
                        <span class="title"><?php echo lang('link_url'); ?></span>
                        <input type="text" disabled="disabled" value="<?php echo $command->link_url; ?>" class="wide_input" />
                        <?php
                        if (!empty($command->link_url)) {
                            echo '<a target="_blank" class="m_left_10" href="' . $command->link_url . '">'
                            . lang('preview') . '</a>';
                        }
                        ?>
                    </li>
                </ul>
            </div>
            <div id="msgtype_music">
                <ul>
                    <li>
                        <span class="title"><?php echo lang('music_url'); ?></span>
                        <input type="text" disabled="disabled" value="<?php echo $command->music_url; ?>" class="wide_input" />
                        <?php
                        if (!empty($command->music_url)) {
                            echo '<a target="_blank" class="m_left_10" href="' . $command->music_url . '">'
                            . lang('preview') . '</a>';
                        }
                        ?>
                    </li>
                    <li>
                        <span class="title"><?php echo lang('hqmusic_url'); ?></span>
                        <input type="text" disabled="disabled" value="<?php echo $command->hqmusic_url; ?>" class="wide_input" />
                        <?php
                        if (!empty($command->hqmusic_url)) {
                            echo '<a target="_blank" class="m_left_10" href="' . $command->hqmusic_url . '">'
                            . lang('preview') . '</a>';
                        }
                        ?>
                    </li>
                </ul>
            </div>
        </li>
        <li class="text_c">
            <input type="button" value="<?php echo lang('back'); ?>" onclick="url_redirect('<?php echo site_url('admin/commands_index'); ?>');" />
        </li>
    </ul>
</div>