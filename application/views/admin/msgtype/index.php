<div id="win_r">
    <table cellpadding="0" cellspacing="0" class="text_c">
        <tr class="table_title">
            <td style="width: 150px"><?php echo lang('msg_type'); ?></td>
            <td style="width: 200px"><?php echo lang('msg_type_desc'); ?></td>
            <td style="width: 100px"><?php echo lang('msg_type_for_reply'); ?></td>
            <td style="width: 150px"><?php echo lang('operation'); ?></td>
        </tr>
        <?php
        if (is_array($msgtype)) {
            foreach ($msgtype as $v) {
                $tmp = '<tr>'
                        . '<td>' . $v->type_name . '</td>'
                        . '<td>' . $v->description . '</td>'
                        . '<td>' . ($v->for_reply ? lang('yes') : lang('no')) . '</td>'
                        . '<td>'
                        . anchor(site_url('admin/msgtype_edit/' . $v->id), lang('edit'))
                        . ' | '
                        . anchor(site_url('admin/msgtype_dodel/' . $v->id), lang('delete'), array(
                            'onclick' => 'if(false===confirm(\'' . lang('confirm_to_delete') . '\')){return false;}'
                        ))
                        . '</td>';
                echo $tmp;
            }
        } else {
            echo lang('err_no_data');
        }
        ?>
    </table>
    <?php echo $pages; ?>
</div>