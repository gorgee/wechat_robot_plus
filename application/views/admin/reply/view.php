<div id="win_r">
    <ul>
        <li>
            <span class="title"><?php echo lang('keyword'); ?></span>
            <span class="m_left_10"><?php echo $reply->for_keyword; ?></span>
        </li>
        <li>
            <span class="title"><?php echo lang('reply_msgtype'); ?></span>
            <span class="m_left_10"><?php echo $reply->msgtype_name; ?></span>
        </li>
        <li>
            <ul>
                <li>
                    <span class="title"><?php echo lang('reply_content'); ?></span>
                    <textarea disabled="disabled" class="input_area"><?php echo $reply->content; ?></textarea>
                </li>
                <li>
                    <span class="title"><?php echo lang('title'); ?></span>
                    <input type="text" disabled="disabled" value="<?php echo $reply->title; ?>" class="wide_input" />
                </li>
                <li>
                    <span class="title"><?php echo lang('description'); ?></span>
                    <textarea disabled="disabled" class="input_area"><?php echo $reply->description; ?></textarea>
                </li>
                <li>
                    <span class="title"><?php echo lang('pic_url'); ?></span>
                    <input type="text" disabled="disabled" value="<?php echo $reply->pic_url; ?>" class="wide_input" />
                    <?php
                    if (!empty($reply->pic_url)) {
                        echo '<a target="_blank" class="m_left_10" href="' . $reply->pic_url . '">'
                        . lang('preview') . '</a>';
                    }
                    ?>
                </li>
                <li>
                    <span class="title"><?php echo lang('link_url'); ?></span>
                    <input type="text" disabled="disabled" value="<?php echo $reply->link_url; ?>" class="wide_input" />
                    <?php
                    if (!empty($reply->link_url)) {
                        echo '<a target="_blank" class="m_left_10" href="' . $reply->link_url . '">'
                        . lang('preview') . '</a>';
                    }
                    ?>
                </li>
                <li>
                    <span class="title"><?php echo lang('music_url'); ?></span>
                    <input type="text" disabled="disabled" value="<?php echo $reply->music_url; ?>" class="wide_input" />
                    <?php
                    if (!empty($reply->music_url)) {
                        echo '<a target="_blank" class="m_left_10" href="' . $reply->music_url . '">'
                        . lang('preview') . '</a>';
                    }
                    ?>
                </li>
                <li>
                    <span class="title"><?php echo lang('hqmusic_url'); ?></span>
                    <input type="text" disabled="disabled" value="<?php echo $reply->hqmusic_url; ?>" class="wide_input" />
                    <?php
                    if (!empty($reply->hqmusic_url)) {
                        echo '<a target="_blank" class="m_left_10" href="' . $reply->hqmusic_url . '">'
                        . lang('preview') . '</a>';
                    }
                    ?>
                </li>
            </ul>
        </li>
        <li class="text_c">
            <input type="button" value="<?php echo lang('back'); ?>" onclick="url_redirect('<?php echo site_url('admin/reply_index'); ?>');" />
        </li> 
    </ul>
</div>