<div id="win_r">
    <table cellpadding="0" cellspacing="0" class="text_c">
        <tr class="table_title">
            <td style="width: 200px"><?php echo lang('keyword'); ?></td>
            <td style="width: 150px"><?php echo lang('reply_msgtype'); ?></td>
            <td style="width: 150px"><?php echo lang('operation'); ?></td>
        </tr>
        <?php
        if (is_array($reply)) {
            foreach ($reply as $v) {
                $tmp = '<tr>'
                        . '<td>' . $v->for_keyword . '</td>'
                        . '<td>' . $v->msgtype_name . '</td>'
                        . '<td>'
                        . anchor(site_url('admin/reply_view/' . $v->id), lang('view'))
                        . ' | '
                        . anchor(site_url('admin/reply_edit/' . $v->id), lang('edit'))
                        . ' | '
                        . anchor(site_url('admin/reply_dodel/' . $v->id), lang('delete'), array(
                            'onclick' => 'if(false===confirm(\'' . lang('confirm_to_delete') . '\')){return false;}'
                        ))
                        . '</td>';
                echo $tmp;
            }
        } else {
            echo lang('err_no_data');
        }
        ?>
    </table>
    <?php echo $pages; ?>
</div>