<div id="win_r">
    <form action="<?php echo site_url('admin/user_doedit'); ?>" method="post">
        <ul>
            <li>
                <span class="title">用户名</span>
                <?php echo $user->username; ?>
                <input type="hidden" name="uid" value="<?php echo $user->id; ?>">
                <input type="hidden" name="username" value="<?php echo $user->username; ?>">
            </li> 
            <li>
                <span class="title">密码</span>
                <input type="password" name="password" class="input" maxlength="20" />
                <span class="m_left_10 notice">可用下划线、字母及数字，不修改请留空</span>
            </li> 
            <li>
                <span class="title">验证密码</span>
                <input type="password" name="re_password" class="input" maxlength="20" />
            </li> 
            <li>
                <span class="title">权限</span>
                <select name="role" class="input">
                    <?php
                    if ('admin' == strval($user->group)) {
                        ?>
                        <option value="0">普通用户</option>
                        <option value="1" selected="selected">管理员</option>
                        <?php
                    } else {
                        ?>
                        <option value="0" selected="selected">普通用户</option>
                        <option value="1">管理员</option>
                        <?php
                    }
                    ?>
                </select>
            </li> 
            <li>
                <input type="submit" value="修改" onclick="submit();" />
            </li> 
        </ul>
    </form>
</div>