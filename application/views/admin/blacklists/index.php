<div id="win_r">
    <table cellpadding="0" cellspacing="0" class="text_c" >
        <tr class="table_title">
            <td style="width: 250px"><?php echo lang('user_opn_id'); ?></td>
            <td style="width: 200px"><?php echo lang('banned_time'); ?></td>
            <td style="width: 100px"><?php echo lang('operation'); ?></td>
        </tr>
        <?php
        if (is_array($blacklists)) {
            foreach ($blacklists as $v) {
                $tmp = '<tr>'
                        . '<td>' . $v->user_opn_id . '</td>'
                        . '<td>' . time2date($v->add_time) . '</td>'
                        . '<td>'
//                        . anchor(site_url('' . $v->user_opn_id), lang('edit'))
//                        . ' | '
                        . anchor(site_url('admin/blacklists_dodel/' . $v->user_opn_id), lang('delete'), array(
                            'onclick' => 'if(false===confirm(\'' . lang('confirm_to_delete') . '\')){return false;}'
                        ))
                        . '</td>';
                echo $tmp;
            }
        } else {
            echo lang('err_no_data');
        }
        ?>
    </table>
    <?php echo $pages; ?>
</div>