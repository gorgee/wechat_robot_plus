<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title> <?php echo (isset($title) ? ($title . ' - ') : '') . lang('site_title'); ?> </title>
        <meta name="Author" content="terminus">
        <meta name="Keywords" content="<?php echo lang('site_keyword'); ?>">
        <meta name="Description" content="<?php echo lang('site_description'); ?>">
        <link type="text/css" href="<?php echo public_res('css/themes/base/jquery.ui.all.css'); ?>" rel="stylesheet" />
        <link type="text/css" href="<?php echo public_res('css/style.css'); ?>" rel="stylesheet" />
        <script type="text/javascript" src="<?php echo public_res('js/jquery-1.9.1.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo public_res('js/jquery-ui.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo public_res('js/fun.js'); ?>"></script>
    </head>

    <body>
        <div id="container">
            <div id="header">
                <ul class="top_menu">
                    <li class="menu-cell"><?php echo lang('ui_admin_hi'); ?>，<?php echo $uname; ?>。</li>
                    <li class="menu-cell m_left_15"><?php echo anchor('admin', lang('ui_admin')); ?></li>
                    <li class="menu-cell m_left_15"><?php echo anchor('admin/logout', lang('logout')); ?></li>
                </ul>
            </div>
            <div id="page_body">