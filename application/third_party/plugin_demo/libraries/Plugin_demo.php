<?php

/**
 * Description of plugin_demo
 *
 * @author terminus
 */
class plugin_demo {

    public function run($userdatas) {
        $userdatas_arr = get_object_vars($userdatas);

        $content = '';
        foreach ($userdatas_arr as $k => $v) {
            $content .= $k . '=' . $v . "\n";
        }

        $reply = new stdClass();
        $reply->reply_msgtype = 'text';
        $reply->content = $content;

        return $reply;
    }

}
