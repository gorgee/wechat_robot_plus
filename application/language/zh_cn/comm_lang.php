<?php

$lang['site_title'] = 'tt的微信机器人 - wechat_robot';
$lang['site_description'] = '功能简单,界面简陋,代码繁乱的微信聊天机器人';
$lang['site_keyword'] = '微信,机器人,wechat,robot,自动,聊天,回复';

$lang['yes'] = '是';
$lang['no'] = '否';
$lang['none'] = '无';
$lang['username'] = '用户名';
$lang['password'] = '密码';
$lang['re_password'] = '验证密码';
$lang['authority'] = '权限';
$lang['common_user'] = '普通用户';
$lang['administrator'] = '管理员';
$lang['login'] = '登录';
$lang['logout'] = '注销登录';
$lang['logout_success'] = '注销登录成功';

$lang['select_file'] = '选择文件';
$lang['upload'] = '上传';
$lang['title'] = '标题';
$lang['notice'] = '提示';
$lang['operation'] = '操作';
$lang['preview'] = '预览';
$lang['back'] = '返回';
$lang['view'] = '查看';
$lang['add'] = '新增';
$lang['edit'] = '编辑';
$lang['delete'] = '删除';
$lang['user_opn_id'] = '用户Id';
$lang['platform_opn_id'] = '平台Id';
$lang['msg_type'] = '消息类型';
$lang['msgtype'] = '消息类型';
$lang['keyword'] = '关键字';
$lang['keywords'] = '关键字';
$lang['description'] = '描述';
$lang['reply'] = '回复';
$lang['msgtype_text'] = '文字类型';
$lang['msgtype_news'] = '图文超链接类型';
$lang['msgtype_music'] = '音频类型';
$lang['command'] = '命令';
$lang['commands'] = '命令';

$lang['use_saved_data_note1'] = '您上次使用的是 ';
$lang['use_saved_data_note2'] = '，若不修改可直接输入';