<?php

/**
 * Description of TT_Controller
 *
 * @author terminus
 */
class TT_Controller extends CI_Controller {

    protected $uid;
    protected $uname;
    protected $ugroup;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('uid') > 0) {
            $this->uid = $this->session->userdata('uid');
            $this->uname = $this->session->userdata('uname');
            $this->ugroup = $this->session->userdata('ugroup');
        } else {
            message(lang('notice'), lang('err_nologin'), site_url('login'));
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        message(lang('notice'), lang('logout_success'), site_url('login'));
    }

    protected function loadview($view, $assign = '') {
        if (is_array($assign)) {
            $assign = array_merge($assign, array(
                'uid' => $this->uid,
                'uname' => $this->uname,
                'ugroup' => $this->ugroup
            ));
        } else {
            $assign = array(
                'uid' => $this->uid,
                'uname' => $this->uname,
                'ugroup' => $this->ugroup
            );
        }
        $this->load->view('inc/header', $assign);
        if (is_array($view)) {
            foreach ($view as $v) {
                $this->load->view($v);
            }
        } else {
            $this->load->view($view);
        }
        $this->load->view('inc/footer');
    }

    public function doupload() {
        $config = array(
            'encrypt_name' => TRUE,
            'upload_path' => up_url(NULL, 1),
            'max_size' => 2048,
            'allowed_types' => 'gif|jpg|png|mp3|wav|wma'
        );

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
//            $datas['result'] = $this->upload->display_errors();
            $datas['result'] = lang('err_file_upload_failed');
        } else {
            $datas['result'] = up_url($this->upload->data()['file_name'], 0);
        }

        //指定父页面接收上传文件名的元素id
        $datas['result_field_id'] = empty($_POST['result_field_id']) ? 'upload_filename' : $_POST['result_field_id'];
        $this->load->view('iframe_uploader_result', $datas);
    }

}
