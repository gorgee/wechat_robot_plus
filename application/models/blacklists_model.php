<?php

/**
 * Description of blacklists_model
 *
 * @author terminus
 */
class Blacklists_model extends CI_Model {

    private $tableName = 'blacklists';

    public function __construct() {
        parent::__construct();
    }

    public function add($user_opn_id) {
        $set = array(
            'user_opn_id' => $user_opn_id,
            'add_time' => time()
        );
        if (FALSE === $this->get($user_opn_id)) {
            $this->db->insert($this->tableName, $set);
            if ($this->db->affected_rows() > 0) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function del($user_opn_id) {
        $where['user_opn_id'] = $user_opn_id;

        $this->db->delete($this->tableName, $where, 1);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function get($user_opn_id) {
        $where['user_opn_id'] = $user_opn_id;

        $result = $this->db->get_where($this->tableName, $where, 1);
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return FALSE;
    }

    public function getAll($only_count = 0, $limit = '', $offset = '') {
        if (1 != $only_count) {
            if ($limit > 0) {
                $this->db->limit($limit);
                if ($offset > 0) {
                    $this->db->limit($limit, $offset);
                }
            }
        }
        $result = $this->db->order_by('add_time', 'DESC')->get($this->tableName);
        if ($result->num_rows() > 0) {
            if (1 != $only_count) {
                return $result->result();
            }
            return $result->num_rows();
        }
        return FALSE;
    }

}

