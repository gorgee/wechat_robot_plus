<?php

/**
 * Description of user_model
 *
 * @author terminus
 */
class User_model extends CI_Model {

    private $tableName = 'user';

    public function __construct() {
        parent::__construct();
    }

    public function getUsers($only_count = 0, $limit = '', $offset = '') {
        if (1 != $only_count) {
            if ($limit > 0) {
                $this->db->limit($limit);
                if ($offset > 0) {
                    $this->db->limit($limit, $offset);
                }
            }
        }
        $result = $this->db->order_by('id', 'DESC')->get($this->tableName);
        if ($result->num_rows() > 0) {
            if (1 != $only_count) {
                return $result->result();
            }
            return $result->num_rows();
        }
        return FALSE;
    }

    public function getUser($user, $is_byid = 0) {
        if (1 == $is_byid) {
            $where['id'] = $user;
        } else {
            $where['username'] = $user;
        }

        $result = $this->db->get_where($this->tableName, $where, 1);
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return FALSE;
    }

    public function setUser($user, $password = '', $gourp = '', $last_login_time = '', $is_byid = 1) {
        if (1 == $is_byid) {
            $where['id'] = $user;
        } else {
            $where['username'] = $user;
        }

        if ('' !== $password) {
            $set['password'] = md5($password);
        }

        $set['group'] = $gourp;
        if ('admin' != $gourp) {
            if (FALSE == $this->_chk($user, $is_byid)) {
                return FALSE;
            }
        }

        if ('' !== $last_login_time) {
            $set['last_login_time'] = $last_login_time;
        }

        if (TRUE === $this->db->update($this->tableName, $set, $where, 1)) {
            return TRUE;
        }
        return FALSE;
    }

    public function addUser($username, $password, $group = '') {
        if ($this->getUser($username)) {
            return FALSE;
        }

        $set = array(
            'username' => $username,
            'password' => md5($password),
            'create_time' => time()
        );

        if ('' !== $group) {
            $set['group'] = $group;
        }

        $this->db->insert($this->tableName, $set);
        $id = $this->db->insert_id();
        if ($id > 0) {
            return $id;
        }
        return FALSE;
    }

    public function delUser($user, $is_byid = 1) {
        if (1 == $is_byid) {
            $where['id'] = $user;
        } else {
            $where['username'] = $user;
        }

        if (FALSE == $this->_chk($user, $is_byid)) {
            return FALSE;
        }

        $this->db->delete($this->tableName, $where, 1);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    private function _chk($user, $is_byid = 1) {
        if (1 == $is_byid) {
            $where['id'] = $user;
        } else {
            $where['username'] = $user;
        }

        $sub_where['group'] = 'admin';
        $admin_num = $this->db->get_where($this->tableName, $sub_where)->num_rows();
        $u = $this->getUser($user, 1);
        if (('admin' == strval($u->group)) && (1 == $admin_num)) {
            return FALSE;
        }
        return TRUE;
    }

}
