<?php

/**
 * Description of reply_model
 *
 * @author terminus
 */
class Reply_model extends CI_Model {

    private $tableName = 'reply';

    public function __construct() {
        parent::__construct();
    }

    public function getAll($only_count = 0, $limit = '', $offset = '') {
        if (1 != $only_count) {
            if ($limit > 0) {
                $this->db->limit($limit);
                if ($offset > 0) {
                    $this->db->limit($limit, $offset);
                }
            }
        }
        $result = $this->db->order_by('id', 'DESC')->get($this->tableName);
        if ($result->num_rows() > 0) {
            if (1 == $only_count) {
                return $result->num_rows();
            }
            return $result->result();
        }
        return FALSE;
    }

    public function getReply($id) {
        $where['id'] = $id;

        $result = $this->db->get_where($this->tableName, $where, 1);
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return FALSE;
    }

    public function getReplys($keyword_id) {
        $where['for_key_id'] = $keyword_id;

        $result = $this->db->get_where($this->tableName, $where);
        if ($result->num_rows() > 0) {
            return $result->result();
        }
        return FALSE;
    }

    public function addReply($for_key_id, $reply_msgtype_id, $content = '', $title = '', $desc = '', $pic_url = '', $link_url = '', $music_url = '', $hqmusic_url = '') {
        $set = array(
            'for_key_id' => $for_key_id,
            'reply_msgtype_id' => $reply_msgtype_id,
            'content' => $content,
            'title' => $title,
            'description' => $desc,
            'pic_url' => $pic_url,
            'link_url' => $link_url,
            'music_url' => $music_url,
            'hqmusic_url' => $hqmusic_url
        );

        $this->db->insert($this->tableName, $set);
        $id = $this->db->insert_id();
        if ($id > 0) {
            return $id;
        }
        return FALSE;
    }

    public function editReply($rid, $for_key_id, $reply_msgtype_id, $content, $title, $desc, $pic_url, $link_url, $music_url, $hqmusic_url) {
        $where['id'] = $rid;
        $set = array(
            'for_key_id' => $for_key_id,
            'reply_msgtype_id' => $reply_msgtype_id,
            'content' => $content,
            'title' => $title,
            'description' => $desc,
            'pic_url' => $pic_url,
            'link_url' => $link_url,
            'music_url' => $music_url,
            'hqmusic_url' => $hqmusic_url
        );

        if (TRUE === $this->db->update($this->tableName, $set, $where, 1)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delReply($rid) {
        $where['id'] = $rid;

        $this->db->delete($this->tableName, $where, 1);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

}
